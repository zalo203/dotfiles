#!/usr/bin/fish

# Supresses fish's intro message
set fish_greeting

### "bat" as manpager
# if [ -f /usr/bin/bat ];
#    set -x MANPAGER "sh -c 'col -bx | bat -l man -p'"
#else if [ -f /usr/bin/batcat ];
#    set -x MANPAGER "sh -c 'col -bx | batcat -l man -p'"
#end

### ALIASES ###

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ];
#    test -r ~/.dircolors && eval "(dircolors -b ~/.dircolors)" || eval "(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
end

# Navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
alias .6='cd ../../../../../..'

# Confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# Some ls aliases
if [ -f /usr/bin/exa ];
    alias ls='exa -l --color=always --group-directories-first';
    alias ll='exa -l --color=always --group-directories-first';
    alias la='exa -al --color=always --group-directories-first';
    alias lt='exa -aT --color=always --group-directories-first'
else
    alias ls='ls -l'
    alias ll='ls -l'
    alias la='ls -alh'
end

# if [ -f /usr/bin/bat ];
#     alias less='bat'
# end

if [ -f /usr/bin/batcat ];
    alias bat='batcat'
#    alias less='bat'
#    alias more='bat'
end

# pacman and yay
alias pacsyu='sudo pacman -Syyu'                 # update only standard pkgs
alias yaysua='yay -Sua --noconfirm'              # update only AUR pkgs (yay)
alias yaysyu='yay -Syu --noconfirm'              # update standard pkgs and AUR pkgs (yay)
alias parsua='paru -Sua --noconfirm'             # update only AUR pkgs (paru)
alias parsyu='paru -Syu --noconfirm'             # update standard pkgs and AUR pkgs (paru)
#alias unlock='sudo rm /var/lib/pacman/db.lck'    # remove pacman lock
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'  # remove orphaned packages

# adding flags
alias df='df -h'                          # human-readable sizes
alias free='free -m'                      # show sizes in MB

## Get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

## Get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# git
alias configadd='config add -u'
alias configcom='config commit -m "Minor changes"'
alias addup='git add -u'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
#alias status='git status'
#alias tag='git tag'
alias newtag='git tag -a'

alias config='/usr/bin/git --git-dir=/home/gonzalo/.cfg/ --work-tree=/home/gonzalo'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
 #alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9>


### RANDOM COLOR SCRIPT ###
if [ -d "/opt/shell-color-scripts/colorscripts" ];
    colorscript random
end

### FUNCTIONS ###

# DIRECTORY CREATION
# usage: mkd <path>
function mkd
        mkdir -p "$argv" && cd "$argv";
end

# ARCHIVE EXTRACTION
# usage: ex <file>
function ex
  if [ -f $1 ];
    switch $1
	case 	*.tar.bz2;   tar xjf $1
	case    *.tar.gz;    tar xzf $1
	case	*.bz2;       bunzip2 $1
	case 	*.rar;       unrar x $1
	case	*.gz;        gunzip $1
	case    *.tar;       tar xf $1
	case    *.tbz2;      tar xjf $1
	case    *.tgz;       tar xzf $1
	case    *.zip;       unzip $1
	case    *.Z;         uncompress $1
	case    *.7z;        7z x $1
	case    *.deb;       ar x $1
	case    *.tar.xz;    tar xf $1
	case    *.tar.zst;   unzstd $1
	case    *.txz;       tar -xvf $1
	case    "*";         echo "'$1' cannot be extracted via ex()"
    end
  else
    echo "'$1' is not a valid file"
  end
end

starship init fish | source
