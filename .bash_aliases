#!/bin/bash

### ALIASES ###

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

# Navigation
alias ..='cd ..'
alias ...='cd ../..'
alias .3='cd ../../..'
alias .4='cd ../../../..'
alias .5='cd ../../../../..'
alias .6='cd ../../../../../..'

# Confirm before overwriting something
alias cp="cp -i"
alias mv='mv -i'
alias rm='rm -i'

# Some ls aliases
if [ -f /usr/bin/exa ]; then
#    alias ls='exa -l --color=always --group-directories-first';
    alias ll='exa -l --color=always --group-directories-first';
    alias la='exa -al --color=always --group-directories-first';
    alias lt='exa -aT --color=always --group-directories-first'
else
    alias ll='ls -l'
    alias la='ls -alh'
fi

if [ -f /usr/bin/bat ]; then
    alias less='bat'
fi

if [ -f /usr/bin/batcat ]; then
#    alias cat='batcat'
    alias less='batcat'
#    alias more='batcat'
fi

# pacman and yay
alias pacsyu='sudo pacman -Syyu'                    # update only standard pkgs
alias yaysua='yay -Sua --noconfirm'                 # update only AUR pkgs (yay)
alias yaysyu='yay -Syu --noconfirm'                 # update standard pkgs and AUR pkgs (yay)
alias parsua='paru -Sua --noconfirm'                # update only AUR pkgs (paru)
alias parsyu='paru -Syu --noconfirm'                # update standard pkgs and AUR pkgs (paru)
#alias unlock='sudo rm /var/lib/pacman/db.lck'      # remove pacman lock
alias pacleanup='sudo pacman -Rns (pacman -Qtdq)'   # remove orphaned packages

# adding flags
alias du='du -h'                        # human-readable sizes
alias df='df -h'                        # human-readable sizes
alias free='free -m'                    # show sizes in MB

## Get top process eating memory
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'

## Get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'

# git
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias configadd='config add -u'
alias configcom='config commit -m "Minor changes"'
alias addup='git add -u'
alias branch='git branch'
alias checkout='git checkout'
alias clone='git clone'
alias commit='git commit -m'
alias fetch='git fetch'
alias pull='git pull origin'
alias push='git push origin'
alias status='git status'
#alias tag='git tag'
alias newtag='git tag -a'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# ROT13 encryption decryption
alias rot13='tr "A-Za-z" "N-ZA-Mn-za-m"'
